# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.1.1

- patch: Add print of variables

## 1.1.0

- minor: Upgrade azure cli + add feature helm namespace
- patch: Add helm 3 support

## 1.0.2

- patch: Update support link and license to Microsoft

## 1.0.1

- patch: Update license to MIT

## 1.0.0

- major: Initial release.
